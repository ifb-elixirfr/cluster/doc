
## Datacenter

The {{ platform.name }} Cluster  is hosted by IDRIS (<https://www.idris.fr/>).

This center has high security standards:

- Access is restricted and strictly controlled (closed zone, "Zone à Régime Restrictif"), including access control system, intrusion detection system, video surveillance and security guard  
- Redundant power supply  
- Reliable air conditioning system  
- Fire protection

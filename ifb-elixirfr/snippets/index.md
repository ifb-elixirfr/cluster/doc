
The IFB Core Cluster is part of the National Network of Compute Resources (NNCR) of the Institut Français de Bioinformatique (IFB).

<a href="https://www.france-bioinformatique.fr/">
<img src="./_imgs/PF/IFB-HAUT-COULEUR-PETIT.png" alt="IFB logo" style="width:500px; margin-left: 60px;"/>
</a>

| Current available services |  |
|--:|---|
| [{{ account_manager.url }}]({{ account_manager.url }})  | Cluster Account Manager |
| {{ cluster.url }}  | High Performance Computing  |
| [{{ rstudio.url }}]({{ rstudio.url }})  | R in your browser with Open OnDemand |
| [{{ jupyterhub.url }}]({{ jupyterhub.url }})  | Jupyter Interactive Notebook with Open OnDemand |
| {{ platform.support_link }} | {{ platform.name }} Cluster support |
| [{{ platform.gitlab }}]({{ platform.gitlab }}) | Contribute ! |
| [https://**community**.france-bioinformatique.fr](https://community.france-bioinformatique.fr) | IFB Mutual help and community support |

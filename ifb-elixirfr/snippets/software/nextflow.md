```bash
wget https://github.com/nf-core/configs/blob/master/conf/ifb_core.config
module load nextflow slurm-drmaa graphviz

# Or let nf-core client download the workflow
srun nextflow run ... -profile ifb_core.config ...

# To launch in background
sbatch --wrap "nextflow run ... -profile ifb_core.config ..."
```

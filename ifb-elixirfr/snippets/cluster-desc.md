
## Hardware

- 16 compute nodes (128 cores, 2 TB RAM)
<small>HPE Apollo 2000 XL225n Gen10+, 2x AMD [EPYC 7662](https://www.amd.com/fr/products/cpu/amd-epyc-7662) (3.1GHz, 64 cores), 3 TB RAM</small>
- 68 compute nodes (28 cores, 256 GB RAM)
<small>DELL C6320, 2x Intel Xeon [E5-2695v3](https://ark.intel.com/content/www/us/en/ark/products/81057/intel-xeon-processor-e5-2695-v3-35m-cache-2-30-ghz.html) (2.3GHz, 14 cores), 256GB RAM</small>
- 1 "fat memory" compute node (64 cores, 3 TB RAM)
<small>DELL R930, 4x Intel Xeon [E7-8860v3](https://ark.intel.com/content/www/us/en/ark/products/84680/intel-xeon-processor-e7-8860-v3-40m-cache-2-20-ghz.html) (2.2GHz, 16 cores), 3 To RAM</small>
- 3 gpu compute nodes (32 cores, 512 GB RAM, 1 NVMe scratch volume each)
<small>DELL R7525, 2x AMD EPYC 7343 [AMD-EPYC-7343](https://www.amd.com/en/products/cpu/amd-epyc-7343) (3,2GHz, 16 cores), 2x graphic processors NVIDIA Ampere A100 40GB [NVIDIA A100] (https://www.nvidia.com/content/dam/en-zz/Solutions/Data-Center/a100/pdf/nvidia-a100-datasheet-us-nvidia-1758950-r4-web.pdf), 512 GB RAM</small>
- 2 login and 1 admin nodes  
<small>DELL R630, 2x Intel Xeon [E5-2620v4](https://ark.intel.com/content/www/us/en/ark/products/92986/intel-xeon-processor-e5-2620-v4-20m-cache-2-10-ghz.html) (2.1GHz, 8 cores), 128 Go RAM</small>

Total of 103 nodes - 8434 cores, 56TB of RAM

- Storage based on DDN Lustre EXAScaler (2 PB)  
<small>SFA400NVXE, ES400NVX and 2xSS9012 expansion enclosures, 11 x 3.84TB SSD, 180 x 16TB HDD</small>
- Local network Ethernet 10Gbits/s
<small>DELL S6000 switch 10/40Gb Ethernet</small>

- Internet access 1Gbits/s

<svg id=racksvg version="1.1" baseProfile="full" xmlns="http://www.w3.org/2000/svg" height="500px" width="287px" preserveAspectRatio="xMaxYMax">
  <image xlink:href="_imgs/20190528_plan_rack_cluster_500x287.png" height="500px" width="287px" preserveAspectRatio="xMaxYMax">
  <title>Rack</title>
  </image>
  <rect class="admin_node" x="14" y="344" width="122" height="12" fill="transparent"><title>Admin node</title></rect>
  <rect class="login_node" x="151" y="344" width="121" height="12" fill="transparent"><title>Login node (core)</title></rect>
  <rect class="storage" x="14" y="367" width="121" height="113" fill="transparent"><title>Storage (R730xd</title></rect>
  <rect class="storage" x="151" y="390" width="121" height="90" fill="transparent"><title>Storage (R730xd)</title></rect>
  <rect class="compute_node" x="14" y="56" width="121" height="267" fill="transparent"><title>Compute nodes (cpu-node-01-48)</title></rect>
  <rect class="compute_node" x="151" y="167" width="121" height="113" fill="transparent"><title>Compute nodes (cpu-node-49-68)</title></rect>
  <rect class="compute_node_bigmem" x="151" y="280" width="121" height="44" fill="transparent"><title>Compute nodes (fat node, cpu-node-69)</title></rect>
  <rect class="sw_ethernet_10g" x="14" y="35" width="121" height="11" fill="transparent"><title>Switch Ethernet 10/40Gbits/s</title></rect>
  <rect class="sw_ethernet_10g" x="151" y="35" width="121" height="11" fill="transparent"><title>Switch Ethernet 10/40Gbits/s</title></rect>
</svg>

## Software layer

The cluster is managed by [Slurm](https://slurm.schedmd.com/ "It's Highly Addictive!") (version 20.11.8).

Scientific software and tools are available through [Environment Modules](http://modules.sourceforge.net/) and are mainly based on [Conda](https://docs.conda.io/en/latest/) packages or [Singularity](https://www.sylabs.io/docs/) images.

Operating System: [CentOS](https://www.centos.org/) (cluster) and sometimes [Ubuntu](https://www.ubuntu.com/)

Around the cluster management: [Nagios Core](https://www.nagios.org/projects/nagios-core/), [Netbox](https://github.com/netbox-community/netbox), [Proxmox VE](https://pve.proxmox.com/), [VMware ESX](https://www.vmware.com/).

Deployment and configuration are powered by [Ansible](https://www.ansible.com/) and [GitLab]({{ platform.gitlab }}).
![Schema orchestration](_imgs/ifb-nncr-core-cluster-orchestration-Ansible-GitLab-simple.png)

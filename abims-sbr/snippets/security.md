
## Datacenter

The {{ platform.name }} is hosted the Station Biologique de Roscoff  (https://www.sb-roscoff.fr).

This center has security standards:

- Access is restricted  
- Redundant power supply  
- Reliable air conditioning system  
- Fire protection

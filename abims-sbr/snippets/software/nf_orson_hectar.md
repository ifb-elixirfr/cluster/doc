
> ***On the ABIMS cluster, containers are already provided in `/shared/software/singularity/images/nextflow/`.***  
>
> ***Please give this path to nextflow so it won't download it again.***

```bash
# Give container's path
cd orson/
ln -s /shared/software/singularity/images/nextflow/*.sif containers/

```

It is also possible to run the pipeline with **Hectar** annotation's tool (for Heterokontes).  

To do it, you need to clone the branch of git repository containing Orson's code adapted with Hectar.  
As the code is not published, Orson will not fetch the container Singularity, so it is important to provide it. (It is already provided in the container path above).

```bash
# Clone repository 
git clone --depth 1 --branch hectar_rebased https://gitlab.ifremer.fr/abims-sbr/orson.git

# Get container
cd orson/
ln -s -s /shared/software/singularity/images/nextflow/*.sif containers/
```

Then run the same command as before, adding `--hectar_enable true` to launch Hectar analysis. (Available only for proteins)

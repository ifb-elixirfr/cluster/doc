
## MetaGOflow: a workflow for marine Genomic Observatories' data analysis

MetaGOflow is a CWL workflow for marine Genomic Observatories' data analysis available at <https://github.com/emo-bon/MetaGOflow/tree/eosc-life-gos>

To run it, load the metagoflow module and follow the instructions.

```bash
module load metagoflow/1.0.1
git clone --depth 1 --branch 1.0.1 https://github.com/emo-bon/MetaGOflow.git
cd MetaGOflow
ln -s /shared/bank/metagoflow/1.0.1 ref-dbs
export CWL_SINGULARITY_CACHE="/shared/software/singularity/images/cwl"
```

You can test the pipeline with:

```bash
srun ./run_wf.sh -s -n osd-short -d short-test-case -f test_input/wgs-paired-SRR1620013_1.fastq.gz -r test_input/wgs-paired-SRR1620013_2.fastq.gz
```

```bash
wget https://github.com/nf-core/configs/blob/master/conf/abims.config
module load nextflow slurm-drmaa graphviz

# Or let nf-core client download the workflow
srun nextflow run ... -profile abims.config ...

# To launch in background
sbatch --wrap "nextflow run ... -profile abims.config ..."
```

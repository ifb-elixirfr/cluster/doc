### Backup

{{ platform.name }} provides a backup on your project directory located in **`/shared/projects/`**

⚠️ To take advantage of this process, you have to follow some rules:

- Only the subdirectories **`archive`**, **`script`** and **`finalresult`** are backed up.
- You must place these subdirectories at the root of your project folder.
- Please be smart in your backups for our finances and the planet.

Example:

```raw
/shared/projects/<my_project>/
├── [...]
├── finalresult
├── script
├── archive
├── [...]
```

### Snapshots

{{ platform.name }} also provides **snapshots** for short term recovery of your data to protect against deletion by error.

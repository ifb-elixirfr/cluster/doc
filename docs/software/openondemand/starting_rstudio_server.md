---
title: Starting a Rstudio server
---

The RStudio Server app let you launch a dedicated RStudio server inside a SLURM job.

You can choose the R version you would like to use in RStudio. The latest versions include a large collection of R packages for data science and biology.

You may choose the cluster resource you need for your RStudio server. Such as project account, partition, number of CPUs, amount of memory.

![Setup_Rstudio_session](../../_imgs/software/openondemand/Setup_Rstudio_session.png)

Click Launch to submit the RStudio server job to the queue. The wait time depends on the current load of the cluster. Requesting smaller, shorter jobs may facilitate shorter wait times.

![Rstudio_starting](../../_imgs/software/openondemand/Rstudio_server_starting.png)

When RStudio is ready click on "**Connect to RStudio Server**". Your RStudio session will be opened in a new tab.

![Rstudio_running](../../_imgs/software/openondemand/Rstudio_server_running.png)

RStudio is ready !

![Rstudio](../../_imgs/software/openondemand/Rstudio.png)

---
title: SLURM examples
---

## Salmon

### 1- Open a script file with any text editor (but not Word)

```bash
nano slurm_salmon.sh
```

### 2- Set the slurm parameters, the [conda] environment and the command itself

```bash
#!/bin/bash
#
#SBATCH -o slurm.%N.%j.out
#SBATCH -e slurm.%N.%j.err
#SBATCH --mail-type END
#SBATCH --mail-user foo.bar@france-bioinformatique.fr
#
#SBATCH --partition fast
#SBATCH --cpus-per-task 6
#SBATCH --mem 5GB

module load salmon

salmon quant --threads $SLURM_CPUS_PER_TASK -i transcripts_index -l A -1 reads1.fq -2 reads2.fq -o transcripts_quant
```

### 3- Submit the job

```bash
sbatch slurm_salmon.sh
```

## Random

### 1- Open a script file with any text editor (but not Word)**

For beginners, we suggest to use `nano`, which has restricted functionalities but is quite intuitive.

```bash
nano slurm_random.sh
```

### 2- Copy/Paste the following script which is writing 10 000 random numbers in a file and then sort them

```bash
#!/bin/bash
#
#SBATCH -p fast                      # partition
#SBATCH -N 1                         # nombre de nœuds
#SBATCH -n 1                         # nombre de cœurs
#SBATCH --mem 100                    # mémoire vive pour l'ensemble des cœurs
#SBATCH -t 0-2:00                    # durée maximum du travail (D-HH:MM)
#SBATCH -o slurm.%N.%j.out           # STDOUT
#SBATCH -e slurm.%N.%j.err           # STDERR

for i in {1..10000}; do
  echo $RANDOM >> SomeRandomNumbers.txt
done

sort -n SomeRandomNumbers.txt > SomeRandomNumbers_sorted.txt
```

Press Ctrl-x to exit nano, then "Y" when nano asks you whether the modified buffer should be saved, then press the "Enter" key to confirm the file name.

### 3- Check the content of the script

```bash
cat slurm_random.sh
```

### 4- Submit the job

```bash
sbatch slurm_random.sh
```

### 5- Check the result

Since this script is running a very basic task, the results should promptly be available.

Check the output files with `ls` and `head`.

Note: these commands can be run on the login node since they are consuming very little computing resources.

```bash
# List the result files
ls -l SomeRandomNumbers*.txt

# Print the 20 first lines of the original random numbers
head -n 20 SomeRandomNumbers.txt

# Print the 20 first lines of the sorted random numbers
head -n 20 SomeRandomNumbers_sorted.txt

# Print the 20 last lines of the sorted random numbers
tail -n 20 SomeRandomNumbers_sorted.txt

```

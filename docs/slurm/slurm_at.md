---
title: SLURM at {{ platform.name }}
---

## The Partitions and resource limits

To request access to partitions on demand such as the bigmem node or the gpu nodes, please submit your request on the community support platform: {{ platform.support_link }}
specifying your login and project name.

⚠️ The values below can change. To check the current situation:

```bash
scontrol show partition
sacctmgr list qos format=Name,Priority,MaxTRESPU%20
```

{% include "slurm/slurm_at_partitions.md" %}

## {{ platform.name }} Cluster Computing nodes

⚠️ The values below can change. To check the current situation:

```bash
sinfo -Ne --format "%.15N %.4c %.7z %.7m" -S c,m,N | uniq
```

{% include "slurm/slurm_at_nodes_cpu.md" %}

{% include "slurm/slurm_at_nodes_gpu.md" %}

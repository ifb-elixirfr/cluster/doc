---
title: Support
---

<!-- markdownlint-disable-next-line MD025 -->
# Support

* Our main support address is {{ platform.support_link }}

* Come discuss different topics with us on [IFB Community](https://community.france-bioinformatique.fr/)

* Most of the softwares are public and documentation is widely available on the Internet

* Check the documentation on [{{ platform.name }} Cluster documentation](index.md).

---
title: Security
---

## Human Health Data

The {{ platform.name }} Cluster is **not** authorized to host **healthcare data**.

L'{{ platform.name }} Cluster n'est **pas habilité** pour héberger des **données de santé à caractère personnel** (certification HDS).

{% include "security.md" %}

## Network

Network access is controlled and restricted by a firewall.

- All access protocols are encrypted.  
- Cluster access protocol is **SSH** (or SFTP).
- Web access is done on **HTTPS**.

## Authentication

The {{ platform.name }} users are managed through a central directory (OpenLDAP).  
The access is limited to authorized users.

The {{ platform.name }} Cluster supports password and public key authentication.

Users or IP with multiple wrong access can be banned.

Password must meet minimum requirements.

Some web services (usegalaxy.fr, community.france-bioinformatique.fr, etc) provide their own authentication method and accept anonymous connections.

## Data access

Data access (home directory or project directory) are granted using access-control list (ACL).

Access is managed by users or groups.

- A project is a group and a directory on the storage.  
- A project can be shared.  
- Each access on a project must be approved by the project owner.

Data access are not logged.

IT administrators ({{ platform.name }} Cluster team support) can access the data but only in cases of security or maintenance.

{% include "data/data_backup.md"%}

### Servers / Services

All servers and services are deployed using Ansible (and configurations are under revision control).  
Main infrastructure services are backed up.

## Data encryption

There is no encryption on the storage.

## Availability

This service is provided as an academic best effort, but without any warranty.

## Monitoring

IT Infrastructure is monitored and the {{ platform.name }} team is notified by email on each warning.

## Contact

{{ platform.name }} Cluster team : {{ platform.support_email }}
